<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Ошибочка</title>
</head>
<body>
<b><h1>Зафиксирована ошибка</h1></b>
<p><b>Код ошибки: </b><?= $errno?></p>
<p><b>Текст ошибки: </b><?= $errstr?></p>
<p><b>Ошибка содержится в файле: </b><?= $errfile?></p>
<p><b>Ошибка содержится в строке: </b><?= $errline?></p>
</body>
</html>