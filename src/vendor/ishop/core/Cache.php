<?php


namespace ishop;


class Cache
{
    //создаем только один класс Cache используя singletone
    use TSingletone;

    //уставнавливает/ создает
    public function set($key, $data, $seconds = 3600)
    { // ключ, данные, время 1 час
        if ($seconds) {
            //создаем контент с ключем дата и поместим туда переданные данные
            $content['data'] = $data;
            //софрмирует конечное время
            $content['end_time'] = time() + $seconds;
            //записываем данные в кэш через пут контентс (1)путь файла , потом мы шифруем переменную $key,
            // в файл под типом txt , сериализуем весь контент в строку  )
            if (file_put_contents(CACHE . '/' . md5($key) . '.txt', serialize($content))) {
                return true;
            }
        }
        return false;
    }

    //получает кэш
    public function get($key)
    {
        //сформирует путь к файлу
        $file = CACHE . '/' . md5($key) . '.txt';
        //если такой файл существует
        if (file_exists($file)) {
            $content = unserialize(file_get_contents($file));
            //проверяем не устарели ли данные
            if (time() <= $content['end_time']) {
                return $content['data'];
            }
            // если файл устарел удаляет файл
            unlink($file);
        }
        //если мы не вернули контент или не существет файла
        return false;
    }

    //удаляет кэш
    public function delete($key)
    {
        //получает путь к файлу
        $file = CACHE . '/' . md5($key) . '.txt';
        if (file_exists($file)) {
            //удаляем данные
            unlink($file);
        }
    }
}
