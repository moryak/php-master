<?php

namespace ishop;

class Registtry
{

    use TSingletone;

    public static $properties = [];

    public static function setProperty($name,$value){
        self::$properties[$name] = $value;
    }

    public function  getProperty ($name){
        if (isset(self::$properties[$name])){ // если это свойство существует
            return self::$properties[$name]; // тогда мы его вернем
        }
        return null; // если не существует, то возвращаем null
    }

    public function getProperties(){
        return self::$properties;    // помощь в дебагинге ,возвращает пропертиес
    }

}