<?php


namespace ishop;


class App
{
    public static $app;

    public function __construct()
    {
        $query = trim($_SERVER['REQUEST_URI'], '/');//— Удаляет пробелы (или другие символы) из начала и конца строки
        session_start(); //— Стартует новую сессию, либо возобновляет существующую
        self::$app = Registtry::instance(); // записываем в апп объект класса регистр
        $this->getParams();
        new ErrorHandler();
        Router::dispatch($query);
    }

    protected function getParams()
    {
        $params = require_once CONF . '/params.php';

        if (!empty($params)) {
            foreach ($params as $k => $v) {
                self::$app->setProperty($k, $v);
            }
        }
    }
}
