<?php

namespace ishop\base;

use ishop\App;

abstract class Controller
{
    public $route; // массив с текущим маршрутом
    public $controller;
    public $model;
    public $view;
    public $prefix;
    public $layout;
    public $data = []; //данные из контроллера в вид
    public $meta = ['title' => '', 'desc' => '', 'keywords' => '']; // мета данные title description keywords

    public function __construct($route)
    {
        $this->route = $route;
        $this->controller = $route['controller'];
        $this->model = $route['controller'];
        $this->view = $route['action'];
        $this->prefix = $route['prefix'];
    }

    public function getView()
    {
        $viewObject = new View($this->route, $this->layout, $this->view, $this->meta);
        $viewObject->render($this->data);

    }

    public function set($data)
    {
        //в массив data перенесем данные
        $this->data = $data;
    }

    //добавить полный набор мета-тегов!!!!!!!!
    public function setMeta($tittle = '', $desc = '', $keywords = '')
    {
        $this->meta['title'] = h($tittle);
        $this->meta['desc'] = h($desc);
        $this->meta['keywords'] = h($keywords);
    }

    function isAjax(){
        return isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && $_SERVER['HTTP_X_REQUESTED_WITH']
            === 'XMLHttpRequest';
    }

    public function loadView($view, $vars = []){
        extract($vars);
        require \APP . "/views/{$this->prefix}{$this->controller}/{$view}.php";
        die;
    }

}
