<?php


namespace ishop;


class Router
{
    protected static $routes = []; //хранится таблица маршрутов тут
    protected static $route = []; // текущий маршурт

    public static function add($regexp, $route = [])
    { //записывает правила в табоицу маршрутов
        self::$routes[$regexp] = $route; // запишем в таблицу маршуртов с ключом $regexp
    }

    public static function getRoutes()
    {
        return self::$routes;  //возращаем нашу таблицу маршрутов

    }

    public static function getRoute()
    { // возвращает текущий маршрут
        return self::$route;
    }

    public static function dispatch($url)
    { // принимает url адрес и что то делает с ни
        $url = self::removeQueryString($url);
        if (self::matchRoute($url)) {
            //вызываем контроллер
            $controller = 'app\controllers\\' . self::$route['prefix'] . self::$route['controller'] . 'Controller';
            if (class_exists($controller)) {
                //создаем объект контроллера
                $controllerObject = new $controller(self::$route);
                $action = self::lowerCamelCase(self::$route['action']) . 'Action';
                if (method_exists($controllerObject, $action)) {
                    //вызываем метод
                    $controllerObject->$action();
                    $controllerObject->getView();
                } else {
                    throw new \Exception("Метод $controller::$action не найден", 404);
                }
            } else {
                throw new \Exception("Контроллер $controller не найден", 404);
            }
        } else {
            //если не найдено соответствие, то
            throw new \Exception("Страница не найдена", 404);
        }
    }

    public static function matchRoute($url)
    { //принимает url адрес и искать соответствие в таблице маршрута
        foreach (self::$routes as $pattern => $route) {
            // будет искать сооветвствие шаблона с url-адресом, если такое будет найдено то переместим в переменную $matches
            if (preg_match("#{$pattern}#i", $url, $matches)) {
                foreach ($matches as $k => $v) {// пройдемся в цикле по массиву matches
                    if (is_string($k)) {  // если это строка
                        $route [$k] = $v; // тогда перенесем в переменную рут
                    }
                }
                if (empty($route['action'])) {
                    $route['action'] = 'index';
                }
                if (!isset($route['prefix'])) {
                    $route['prefix'] = '';
                } else {
                    $route['prefix'] .= '\\';
                }
                //поднимаем первую букву
                $route['controller'] = self::upperCamelCase($route['controller']);
                self::$route = $route;  //сохраним в свойстве $route в котором хранится текущий маршрут
                return true;
            }
        }
        return false; // если ничего не найдено вернем false
    }

    //CamelCase
    protected static function upperCamelCase($name)
    {
        //  убираем дефис вместо него пробел^ потом поднмимает первые буквы потом соеденеям все это
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $name)));
        //debug($name);
    }

    // camelCase
    protected static function lowerCamelCase($name)
    {   //маленькая первая буква с АпперКамелКейса
        return lcfirst(self::upperCamelCase($name));
    }

    protected static function removeQueryString($url){
        if($url){
            //$params = explode('&', $url, 2); // закомментировал эту строку
            $params = explode('?', $url, 2); // заменил ее этой
            if(false === strpos($params[0], '=')){
                return rtrim($params[0], '/');
            }else{
                return '';
            }
        }
    }

    /*protected static function removeQueryString($url)
    {
        if ($url) {
            //по символу & мы разделим наш GET параметр на 2 части
            $params = explode('&', $url, 2);

            // проверяем нулевой элемент массива
            if (false === strpos($params[0], '=')) {
                return rtrim($params[0], '/');
            } else {
                return '';
            }
            //debug($params);
        }
    }*/
}