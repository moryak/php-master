<?php

namespace app\controllers;

use ishop\Cache;

class MainController extends AppController
{

    public function indexAction()
    {

        // находим все бренды в базе данных под названием brand
        $brands = \R::findAll('brand','LIMIT 3');
        $hits = \R::find('product', "hit = '1' AND status = '1' LIMIT 8");
        $canonical = PATH;
        $this->setMeta('Главная страница', 'Описание...', 'Ключевики...');
        //вывод брендов на страницу
        $this->set(compact('brands', 'hits','canonical'));



    }


}