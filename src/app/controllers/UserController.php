<?php


namespace app\controllers;


use app\models\User;

class UserController extends AppController
{
    public function signupAction(){
        if (!empty($_POST)){
            $user = new User();
            $data = $_POST;
            $user->load($data);
            if(!$user->validate($data) || !$user->checkUnique()){
                $user->getErrors(); // записываем ошибки в сессию
                $_SESSION['form_data'] = $data; //запомниаем чтобы при обнолвеннии не пропадали
            }else{
                //шифруем пароль
                $user->attributes['password'] = password_hash($user->attributes['password'], PASSWORD_DEFAULT);
                if ($user->save('user')){ //сохраняем данные в таблицу User
                    $_SESSION['success'] ='Пользователь зарегестрирован';
                }else{
                    $_SESSION['error'] ='Ошибка!';
                }
            }
            redirect(); // перезапустим страницу
        }
        $this->setMeta('Registration');
    }

    public function loginAction(){
        if(!empty($_POST)){
            $user = new User();
            if ($user->login()){
                $_SESSION['success'] = 'Вы успешно авторизованы';
            }else{
                $_SESSION['error'] = 'Логин/пароль введены неверно';
            }
            redirect();
        }
    $this->setMeta('Вход');
    }

    public function logoutAction(){
        if(isset($_SESSION['user'])) unset($_SESSION['user']);
        redirect();
    }
}