<?php
// ошибка находится на этой странице
namespace app\controllers;

use app\models\Cart;
use RedBeanPHP\R;

class CurrencyController extends AppController{

    public function changeAction(){
        $currency = !empty($_GET['curr']) ? $_GET['curr'] : null;
        if($currency){
            $curr = \R::findOne('currency', 'code = ?', [$currency]);
            if (!empty($curr)){
                setcookie('currency', $currency, time()+3600*24*7, '/');
                Cart::recalc($curr); //перерасчет валют
            }
        }
        redirect();
    }


}

// вот это вверху браузера "Главная страница" - это что такое?))
// это тайтл почему то он там так встал и стоит:D
/*
 * это не тайтл... это какой-то посторонний вывод в браузер, который, к слову и может быть источником проблемы
 *
 * */
