<?php
/**
 * ПРОБЛЕМА ТОЧНО В ЭТОЙ СТРАНИЦЕ
 */

namespace app\widgets\currency;


use ishop\App;

class Currency
{

    protected $tpl; // шаблон выпадающего списка
    protected $currencies;
    protected $currency; //активная для пользователя валюта


    public function __construct()
    {
        $this->tpl = __DIR__ . '/currency_tpl/currency.php';
        $this->run();
    }

    protected function run()
    {
        // СЮДА НЕ ЗАХОДИТ
        $this->currencies = App::$app->getProperty('currencies');
        $this->currency = App::$app->getProperty('currency');
        //var_dump('i\'m here!');
        echo $this->getHtml();
    }

    // забираем данные с БД
    public static function getCurrencies()
    {
        return \R::getAssoc("SELECT code, title, symbol_left, symbol_right, value, base FROM currency ORDER BY base DESC ");
    }
    //получение масссива currency
    public static function getCurrency($currencies)
    {

        //проверяем сущетсвует ли в куках currency и существует ли такой ключ в куках с именем $currencies
        if (isset($_COOKIE['currency']) && array_key_exists($_COOKIE['currency'], $currencies)) {
            //если да то запоминаем в переменную $key
            $key = $_COOKIE['currency'];
        } else {
            // возвращает текущий элемент массива
            $key = key($currencies);
        }
        $currency = $currencies[$key];
        $currency['code'] = $key;
        return $currency;
    }

    //формирует HTML разметку| подключает шаблон
    protected function getHtml()
    {
        ob_start();
        debug(ob_start());
        require_once $this->tpl;
        /*var_dump('i\'m here!');*/
        //debug($this->tpl); // CHECK IT
        return ob_get_clean();
    }
}