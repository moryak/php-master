<?php

define("DEBUG", 1);//отвечаает за режим работы
define("ROOT", dirname(__DIR__)); //указывает корень сайта
define("WWW", ROOT . '/public');//указывает на публичную папку
define("APP", ROOT . '/app');
define("CORE", ROOT . '/vendor/ishop/core');
define("LIBS", ROOT . '/vendor/ishop/core/libs');
define("CACHE", ROOT . '/tmp/cache');
define("CONF", ROOT . '/config');
define("LAYOUT", 'watches'); //хранится шаблон нашего сайта по умолчанию  имя default

//http://localhost:8000 /index.php
$app_path = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['PHP_SELF']}";

//http://localhost:8000 /
$app_path = preg_replace("#[^/]+$#", '', $app_path); // вырезаем наименование скрипта СКРЫВАЕМ Index.php

$app_path = str_replace('/public/', '', $app_path);

define("PATH", $app_path);
define("ADMIN", PATH . 'admin');

require_once ROOT . '/vendor/autoload.php';